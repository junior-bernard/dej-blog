class WelcomeController < ApplicationController
    def index
        render "/welcome/index.html.haml"
    end
end
